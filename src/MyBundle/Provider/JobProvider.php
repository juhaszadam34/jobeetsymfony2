<?php

namespace MyBundle\Provider;

use Doctrine\ORM\NonUniqueResultException;
use Exporter\Exception\InvalidMethodCallException;
use MyBundle\Entity\Job;
use MyBundle\Manager\JobManager;
use MyBundle\Manager\Manager;

class JobProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * @param null|int|string $category_id
     * @param null|int        $max
     * @param null|int        $offset
     * @param null|int|string $affiliate_id
     * @return Job[]
     * @throws InvalidMethodCallException
     */
    public function getActiveJobs($category_id = null, $max = null, $offset = null, $affiliate_id = null)
    {
        if (
            $this->manager instanceof JobManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->getActiveJobs($category_id, $max, $offset, $affiliate_id);
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }

    /**
     * @param null|int|string $category_id
     * @return int
     * @throws InvalidMethodCallException
     */
    public function countActiveJobs($category_id = null)
    {
        if (
            $this->manager instanceof JobManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->countActiveJobs($category_id);
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }

    /**
     * @param int|string $id
     * @return Job|null
     * @throws NonUniqueResultException
     * @throws InvalidMethodCallException
     */
    public function getActiveJob($id)
    {
        if (
            $this->manager instanceof JobManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->getActiveJob($id);
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }

    /**
     * @return Job|null
     * @throws NonUniqueResultException
     * @throws InvalidMethodCallException
     */
    public function getLatestPost()
    {
        if (
            $this->manager instanceof JobManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->getLatestPost();
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }
}
