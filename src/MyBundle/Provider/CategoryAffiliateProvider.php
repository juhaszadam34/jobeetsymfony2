<?php

namespace MyBundle\Provider;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exporter\Exception\InvalidMethodCallException;
use MyBundle\Entity\Category;
use MyBundle\Manager\CategoryAffiliateManager;
use MyBundle\Manager\Manager;

class CategoryAffiliateProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * @return Category[]
     * @throws InvalidMethodCallException
     */
    public function getWithJobs()
    {
        if (
            $this->manager instanceof CategoryAffiliateManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->getWithJobs();
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }

    /**
     * @param $token
     * @return mixed|null
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws InvalidMethodCallException
     */
    public function getForToken($token)
    {
        if (
            $this->manager instanceof CategoryAffiliateManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->getForToken($token);
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }
}
