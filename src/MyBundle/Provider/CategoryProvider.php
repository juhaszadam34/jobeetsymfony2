<?php

namespace MyBundle\Provider;

use Exporter\Exception\InvalidMethodCallException;
use MyBundle\Entity\Category;
use MyBundle\Manager\CategoryManager;
use MyBundle\Manager\Manager;

class CategoryProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * @return Category[]
     * @throws InvalidMethodCallException
     */
    public function getWithJobs()
    {
        if (
            $this->manager instanceof CategoryManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->getWithJobs();
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findOneBySlug($slug)
    {
        if (
            $this->manager instanceof CategoryManager ||
            $this->manager instanceof Manager
        ) {
            return $this->manager->findOneBySlug($slug);
        } else {
            throw new InvalidMethodCallException("Invalid method call!");
        }
    }
}
